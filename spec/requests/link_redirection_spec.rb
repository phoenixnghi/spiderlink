require 'rails_helper'

RSpec.describe 'Link redirection', type: :request do
  before do
    @user = FactoryBot.create(:user)
    sign_in @user
    # allow(controller).to receive(:authenticate_user!).and_return(true)
    # allow(controller).to receive(:current_user).and_return(@user)
  end

  it 'redirects to the original URL when accessing the shortened link' do
    url       = 'https://www.speedtest.net/'
    link      = @user.links.create(url: url)
    init_hits = link.hits

    get "http://localhost:3000/l/#{link.code}"

    expect(response).to redirect_to(link.url)
    expect(link.reload.hits).to eq(init_hits + 1)
  end

  it 'redirects to home when accessing the wrong shortened link' do
    get "http://localhost:3000/l/zzzzzzz"

    expect(response).to redirect_to('/')
  end

end
