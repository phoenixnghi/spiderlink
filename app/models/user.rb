class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  has_many :links

  validates_length_of :api_key, maximum: 20

  before_save :gen_api_key

  private

  def gen_api_key
    self.api_key = Devise.friendly_token
  end
end
