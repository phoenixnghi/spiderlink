require 'rails_helper'

RSpec.describe Shortener do

  it 'generates 7 char code' do
    code = Shortener.new.code
    expect(code.size).to eq(7)
  end

end
