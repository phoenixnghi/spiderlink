# README

## CORE REQUIREMENTS

### As a visitor, I want to create an account for the app, so that I become an active user.
- [x] Application needs my email, and password. 
- [x] Duplicated emails are not allowed. 
- [x] Validates email input.


### As an active user I must be able to put a URL into the home page and get back a URL of the shortest possible length.
- [x] You shouldn't use any gem or library that provides the short url algorithm
- [x] Inputting a valid URL should result in displaying the new shortened URL to the user.
- [x] Inputting an invalid URL should result in displaying errors to the user.
- [x] As an active user I must be able to manage my links, edit, create, destroy, etc.
- [x] As an active user, every time I share this link, I must be redirected to the full URL when we enter the short URL
(ex: http://app.com/aSdF => https://google.com).
- [x] As an active user I want to see a list of my links, ordered by creation date.
- [x] As an active user I want to click on my links to see details and stats.
- [x] I want to see how many times my link was clicked.
- [x] Bonus point if you test ~~all~~ your code with RSpec.
- [x] There must be a view for the Top 100 most frequently accessed URLs.

### As an active user I want an API key to access and integrate any other platform with this shortener.
- [x] I want an endpoint to access all my links.
- [x] All endpoint results must be paginated.
```
curl --location --request GET 'localhost:3000/api/links?per=10&page=1' \
--header 'X-API-VERSION: v1' \
--header 'Authorization: users.api_key'
```
- [x] I want an endpoint to shorten new URLs.
```
curl --location --request POST 'localhost:3000/api/links' \
--header 'X-API-VERSION: v1' \
--header 'Authorization: users.api_key' \
--header 'Content-Type: application/x-www-form-urlencoded' \
--data-urlencode 'url=https://www.speedtest.net/'
```

## HOW TO SETUP
### DB
- sqlite3

### ENVs
NO_OF_TOP_HITS=100 (top 100 most visited URL on the homepage)

### Algorithm
- Create a random UUID and take the first 7 characters
```ruby
SecureRandom.uuid[0..6]
```

### Docker
- Support Docker
