class CreateLinks < ActiveRecord::Migration[6.1]
  def change
    create_table :links do |t|
      t.text :url, null: false
      t.string :code, uniq: true, null: false
      t.integer :hits, default: 0
      t.references :user, null: true, foreign_key: true

      t.timestamps
    end
  end
end
