# frozen_string_literal: true

class Api::V1::BaseController < ApplicationController
  include AuthorizationHelper

  skip_before_action :verify_authenticity_token

  def params_paging
    params.permit(:page, :per).to_h.symbolize_keys
  end
end
