# frozen_string_literal: true

module ResponseHelper
  extend ActiveSupport::Concern

  def json_with_success(message: :success, data: nil)
    {
      code:    200,
      message: message,
      data:    data
    }
  end

  def json_with_error(message: :fail, errors: nil)
    {
      message: message,
      errors:  errors
    }
  end

  def json_with_pagination(data)
    pagination = {
      limit_value:  data.respond_to?(:limit_value) && data.limit_value ? data.limit_value : 0,
      current_page: data.respond_to?(:current_page) ? data.current_page : 1,
      next_page:    data.respond_to?(:next_page) ? data.next_page : nil,
      prev_page:    data.respond_to?(:prev_page) ? data.prev_page : nil,
      total_pages:  data.respond_to?(:total_pages) ? data.total_pages : 1
    }

    {
      pagination: pagination,
      items:      data
    }
  end

end
