# frozen_string_literal: true

module ErrorsHandler
  extend ActiveSupport::Concern

  class Forbidden < StandardError; end

  included do
    rescue_from Forbidden, with: :handle_forbidden
    rescue_from ActionController::ParameterMissing, with: :handle_parameter_missing
    rescue_from ActiveRecord::RecordInvalid, with: :handle_record_invalid
  end

  protected

  def handle_forbidden
    render json: json_with_error(message: I18n.t("errors.forbidden")), status: :forbidden
  end

  def handle_parameter_missing
    render json: json_with_error(message: I18n.t("errors.parameters_missing")), status: :bad_request
  end

  def handle_record_invalid(exception)
    render json: json_with_error(
                   message: I18n.t("errors.record_invalid", record: humanized_model_name(exception.record.class.to_s)),
                   errors:  exception.record&.errors&.messages
                 ), status: :unprocessable_entity
  end

  private

  def humanized_model_name(model_name)
    model_name&.underscore&.humanize(keep_id_suffix: true) || "record"
  end
end
