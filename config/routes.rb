Rails.application.routes.draw do
  devise_for :users

  resources :links

  get 'home/index'
  root 'home#index'

  get '/l/:code' => "links#redirect"

  extend ApiRoutes
end
