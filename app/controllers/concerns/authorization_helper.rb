require 'json'

module AuthorizationHelper
  extend ActiveSupport::Concern

  protected

  def authorize_current_user
    return current_user if current_user

    raise ErrorsHandler::Forbidden
  end

  def current_user
    User.find_by(api_key: header_token)
  end

  def header_token
    request.headers['HTTP_AUTHORIZATION']
  end

end
