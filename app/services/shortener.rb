# frozen_string_literal: true

class Shortener

  def code
    loop do
      code = gen_code
      break code unless Link.exists?(code: code)
    end
  end

  private

  def gen_code
    SecureRandom.uuid[0..6]
  end

end
