# frozen_string_literal: true
require 'rails_helper'

RSpec.describe Link, type: :model do
  it { is_expected.to validate_presence_of(:url) }

  it { should validate_length_of(:code).is_at_most(7) }
  it { should validate_length_of(:url).is_at_most(300) }

  it { should validate_uniqueness_of(:code) }

  it 'is invalid if the URL is in wrong format' do
    link = Link.new(url: 'zzz')
    expect(link.valid?).to be(false)
  end
end
