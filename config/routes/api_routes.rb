# frozen_string_literal: true

module ApiRoutes
  def self.extended(router)
    router.instance_exec do
      namespace :api do
        api_version(module: 'V1', header: { name: 'X-API-VERSION', value: 'v1' }) do
          resource :links, only: [:create] do
            get '/', to: 'links#index'
          end
        end
      end
    end
  end
end
