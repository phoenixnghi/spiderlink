class LinksController < ApplicationController
  before_action :authenticate_user!, except: :redirect
  before_action :set_link, only: %i[show edit update destroy]

  # GET /links or /links.json
  def index
    @links = Link.where(user: current_user).order(created_at: :desc)
  end

  # GET /links/1 or /links/1.json
  def show
  end

  # GET /links/new
  def new
    @link = Link.new
  end

  # GET /links/1/edit
  def edit
  end

  # POST /links or /links.json
  def create
    @link = current_user.links.create(link_params)

    if @link.persisted?
      @shortened_url = "#{request.base_url}/l/#{@link.code}"
      respond_to :js
    else
      render 'error.js.erb'
    end
  end

  # PATCH/PUT /links/1 or /links/1.json
  def update
    if @link.update(link_params)
      @shortened_url = "#{request.base_url}/l/#{@link.code}"
      render 'show'
    else
      render 'error.js.erb'
    end
  end

  # DELETE /links/1 or /links/1.json
  def destroy
    @link.destroy

    respond_to do |format|
      format.html { redirect_to links_url, flash: {info: "Link was successfully destroyed."} }
      format.json { head :no_content }
    end
  end

  def redirect
    link = Link.find_by(code: params[:code])
    return redirect_to '/' unless link

    link.increment!(:hits)

    redirect_to link.url
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_link
    @link = Link.find(params[:id])
  end

  def link_params
    params.require(:link).permit(:url)
  end
end
