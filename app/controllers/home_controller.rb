class HomeController < ApplicationController

  def index
    @top_links = Link.top_links
  end

end
