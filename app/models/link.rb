# frozen_string_literal: true

class Link < ApplicationRecord

  belongs_to :user

  validates_presence_of :url
  validates_length_of :code, maximum: 7
  validates_length_of :url, maximum: 300
  validates_uniqueness_of :code
  validate :validate_url_format

  scope :top_links, -> { order(hits: :desc).limit(ENV.fetch('NO_OF_TOP_HITS', 100)) }

  before_save :gen_code

  def validate_url_format
    uri = URI.parse(url || '')
    errors.add(:url, I18n.t('links.invalid_format')) if uri.host.nil?
  end

  private

  def gen_code
    self.code = Shortener.new.code
  end
end
