# frozen_string_literal: true

require 'rails_helper'

URL   = 'https://www.speedtest.net/'.freeze
URL_2 = 'https://www.speedtest.com/'.freeze

RSpec.describe LinksController, type: :controller do
  before do
    @user = FactoryBot.create(:user)
    sign_in @user
    # allow(controller).to receive(:authenticate_user!).and_return(true)
    # allow(controller).to receive(:current_user).and_return(@user)
  end

  it 'returns shorten link' do
    request.env["HTTP_ACCEPT"] = 'text/javascript'

    post :create, params: { link: { url: URL } }
    link = assigns(:link)
    expect(link.url).to eq(URL)
  end

  it 'shortens a given URL to a 7 char code' do
    link = @user.links.create(url: URL)
    expect(link.code.length).to eq(7)
  end

  it 'gives each URL its own code' do
    link   = @user.links.create(url: URL)
    code_1 = link.code

    link   = @user.links.create(url: URL_2)
    code_2 = link.code

    expect(code_1).not_to eq code_2
  end

  it 'gives different code for same URL' do
    link   = @user.links.create(url: URL)
    code_1 = link.code

    link   = @user.links.create(url: URL_2)
    code_2 = link.code

    expect(code_1).not_to eq code_2
  end

  it 'generates Link record' do
    link = @user.links.create(url: URL)
    expect(link.url).to eq(URL)
  end

end
