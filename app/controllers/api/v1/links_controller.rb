# frozen_string_literal: true

class Api::V1::LinksController < Api::V1::BaseController
  before_action :authorize_current_user

  def index
    links = Link.where(user: current_user).order(created_at: :desc).page(params_paging[:page]).per(params_paging[:per])
    render json: json_with_pagination(links)
  end

  def create
    link = current_user.links.create!(url: link_params[:url])
    render json: json_with_success(data: link)
  end

  private

  def link_params
    params.permit(:url)
  end

end
